University of Twente templates
==============================
The main documentation for the packages is located in [`doc/utwente/utwente.pdf`](https://git.snt.utwente.nl/visual-identity/latex-utwente/raw/master/doc/latex/utwente/utwente.pdf). There you can find usage and install instructions.

Two packages are provided:

 * `utwentetitle`: for a cover page in the University's visual identity.
 * `utwentefont`: for the University font.

A document class is also included:

 * `utwente-beamer`: for presentations.

 This one, however, isn't finished.
